# frozen_string_literal: true

Deface::Override.new(
  virtual_path: "spree/admin/products/_form",
  name: "add_product_tags_field",
  insert_bottom: '[data-hook="admin_product_form_option_types"]',
  partial: "spree/admin/shared/product_tag_form"
)