require "make_taggable"
module Spree
  module ProductDecorator

    def self.prepended(base)
      base.make_taggable :tags
    end
    def self.by_tag(tag_name)
      tagged_with(tag_name, on: :tags)
    end
  end
end

::Spree::Product.prepend(Spree::ProductDecorator)
